import React from 'react';

const MovieList = (props) => {
    const FavouriteComponent = props.favouriteComponent;
    return (
        <>
            {props.movies.map((movie, index)=> (
                <div key={movie.imdbID} className="movie-wrap image-container d-flex justify-content-start m-3">
                    <img src={movie.Poster} alt="movie"></img>
                    <div className="overlay d-flex align-items-center justify-content-start"
                        onClick={()=>props.handleFavouriteClick(movie)} >
                        <h2>{movie.Title}</h2>
                        <FavouriteComponent />
                    </div>
                </div>
            ))}
        </>
    );
};

export default MovieList;